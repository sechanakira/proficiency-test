package zw.co.shingirai.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import zw.co.shingirai.config.BeanConfig;
import zw.co.shingirai.logic.FileOperations;

import java.io.IOException;
import java.nio.file.Path;

import static org.junit.Assert.assertEquals;

/**
 * Created by shingirai on 10/10/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={BeanConfig.class})
public class ProficiencyTestUnitTests {

	@Autowired
	private FileOperations fileOperations;

	@Autowired
	private Path path;

	@Test
	public void contextLoads() {
	}

	@Test
	public void testFileRead() throws IOException {
		assertEquals(985,fileOperations.readTransactions(path).size());
	}

	@Test
	public void testReadContents() throws IOException {
		assertEquals("3526 HIGH ST",fileOperations.readTransactions(path).get(0).getStreet());
	}

}
