package zw.co.shingirai.constants;

/**
 * Created by shingirai on 10/10/16.
 */
public class ApplicationConstants {

	private ApplicationConstants(){
	}

	public static class RealEstateTransactionAttributeConstants{

		public static final String ATTRIBUTE_DELIMITER = ",";
		public static final int STREET_ATTRIBUTE = 0;
		public static final int CITY_ATTRIBUTE = 1;
		public static final int ZIP_ATTRIBUTE = 2;
		public static final int STATE_ATTRIBUTE = 3;
		public static final int BEDS_ATTRIBUTE = 4;
		public static final int BATHS_ATTRIBUTE = 5;
		public static final int AREA_ATTRIBUTE = 6;
		public static final int RESIDENCE_TYPE_ATTRIBUTE = 7;
		public static final int SALE_DATE_ATTRIBUTE = 8;
		public static final int PRICE_ATTRIBUTE = 9;
		public static final int LATITUDE_ATTRIBUTE = 10;
		public static final int LONGITUDE_ATTRIBUTE = 11;

		private RealEstateTransactionAttributeConstants(){
		}
	}

	public static class DateConstants{

		public static final String DATE_PARSE_REGEX = "E MMM dd HH:mm:ss z yyyy";

		private DateConstants(){
		}
	}

	public static class FileConstants{

		public static final String TRANSACTIONS_FILE_PATH = "realestatetransactions.csv";

		private FileConstants(){
		}
	}

}
