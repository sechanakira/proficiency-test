package zw.co.shingirai.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import zw.co.shingirai.logic.FileOperations;
import zw.co.shingirai.model.RealEstateTransaction;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

/**
 * Created by shingirai on 10/10/16.
 */
@RestController
public class RealEstateTransactionEndpoint {

	@Autowired
	private FileOperations fileOperations;

	@Autowired
	private Path filePath;

	@GetMapping("/getList")//REST endpoint
	public @ResponseBody List<RealEstateTransaction> getList() throws IOException {
		return fileOperations.readTransactions(filePath);
	}
}
