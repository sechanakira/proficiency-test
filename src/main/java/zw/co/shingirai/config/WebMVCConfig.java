package zw.co.shingirai.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by shingirai on 10/10/16.
 */
@EnableWebMvc
@Configuration
@ComponentScan(basePackages = "zw.co.shingirai")
public class WebMVCConfig {
}
