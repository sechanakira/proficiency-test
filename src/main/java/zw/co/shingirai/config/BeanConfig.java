package zw.co.shingirai.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import zw.co.shingirai.logic.FileOperationsImpl;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static zw.co.shingirai.constants.ApplicationConstants.FileConstants.TRANSACTIONS_FILE_PATH;

/**
 * Created by shingirai on 10/10/16.
 */
@Configuration
public class BeanConfig {

	@Bean
	public FileOperationsImpl fileOperationsImpl(){
		return new FileOperationsImpl();
	}

	@Bean
	public Path filePath() throws URISyntaxException {
		ClassLoader classLoader = getClass().getClassLoader();
		return Paths.get(classLoader.getResource(TRANSACTIONS_FILE_PATH).toURI());
	}
}
