package zw.co.shingirai.model;

/**
 * Created by shingirai on 10/10/16.
 */
public enum ResidenceType {
	//Residential types
	RESIDENTIAL,
	CONDO,
	MULTIFAMILY,
	UNKNOWN
}
