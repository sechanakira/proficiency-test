package zw.co.shingirai.logic;

import zw.co.shingirai.model.RealEstateTransaction;
import zw.co.shingirai.model.ResidenceType;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static zw.co.shingirai.constants.ApplicationConstants.DateConstants.DATE_PARSE_REGEX;
import static zw.co.shingirai.constants.ApplicationConstants.RealEstateTransactionAttributeConstants.*;

/**
 * Created by shingirai on 10/10/16.
 */
public class FileOperationsImpl implements FileOperations {

	@Override
	public List<RealEstateTransaction> readTransactions(Path path) throws IOException {
		List<RealEstateTransaction> realEstateTransactions = new ArrayList<>();
		Files.lines(path)
						.skip(1) //Skipping first line containing headers
						.forEach(line-> realEstateTransactions.add(buildRealEstateTransaction(line)));
		return realEstateTransactions;
	}

	private ResidenceType getResidenceType(String residenceType){
		switch (residenceType){
			case "Residential":
				return ResidenceType.RESIDENTIAL;
			case "Condo":
				return ResidenceType.CONDO;
			case "Multi-Family":
				return ResidenceType.MULTIFAMILY;
			default:
				return ResidenceType.UNKNOWN;
		}
	}

	private RealEstateTransaction buildRealEstateTransaction(String line){
		String[] realEstateTransactionComponents = line.split(ATTRIBUTE_DELIMITER);
		RealEstateTransaction realEstateTransaction = new RealEstateTransaction();
		realEstateTransaction.setStreet(realEstateTransactionComponents[STREET_ATTRIBUTE]);
		realEstateTransaction.setCity(realEstateTransactionComponents[CITY_ATTRIBUTE]);
		realEstateTransaction.setZip(realEstateTransactionComponents[ZIP_ATTRIBUTE]);
		realEstateTransaction.setState(realEstateTransactionComponents[STATE_ATTRIBUTE]);
		realEstateTransaction.setBeds(Integer.parseInt(realEstateTransactionComponents[BEDS_ATTRIBUTE]));
		realEstateTransaction.setBaths(Integer.parseInt(realEstateTransactionComponents[BATHS_ATTRIBUTE]));
		realEstateTransaction.setTotalArea(Integer.parseInt(realEstateTransactionComponents[AREA_ATTRIBUTE]));
		realEstateTransaction.setResidenceType(getResidenceType(realEstateTransactionComponents[RESIDENCE_TYPE_ATTRIBUTE]));
		realEstateTransaction.setSaleDate(getSaleLocalDateTime(realEstateTransactionComponents[SALE_DATE_ATTRIBUTE]));
		realEstateTransaction.setPrice(Integer.parseInt(realEstateTransactionComponents[PRICE_ATTRIBUTE]));
		realEstateTransaction.setLatitude(Double.parseDouble(realEstateTransactionComponents[LATITUDE_ATTRIBUTE]));
		realEstateTransaction.setLongitude(Double.parseDouble(realEstateTransactionComponents[LONGITUDE_ATTRIBUTE]));
		return realEstateTransaction;
	}

	private LocalDateTime getSaleLocalDateTime(String date){
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_PARSE_REGEX);
		return LocalDateTime.parse(date,dateTimeFormatter);
	}
}
