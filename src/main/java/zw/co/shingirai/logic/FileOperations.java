package zw.co.shingirai.logic;

import zw.co.shingirai.model.RealEstateTransaction;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

/**
 * Created by shingirai on 10/10/16.
 */
@FunctionalInterface
public interface FileOperations {

	List<RealEstateTransaction> readTransactions(Path path) throws IOException;

}
